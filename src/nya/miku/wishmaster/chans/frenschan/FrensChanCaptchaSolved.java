package nya.miku.wishmaster.chans.frenschan;

import android.support.annotation.NonNull;

import java.util.LinkedList;

public class FrensChanCaptchaSolved {

    private static LinkedList<FrensChanCaptcha> solutions = new LinkedList();

    public static FrensChanCaptcha pop() {
        FrensChanCaptcha result = null;
        if (!solutions.isEmpty()) {
            result = solutions.remove();
        }
        return result;
    }

    public static void push(String value, String cookie) {
        solutions.add(new FrensChanCaptcha(value, cookie));
    }

    public static class FrensChanCaptcha {

        @NonNull
        public final String captchaText;
        @NonNull
        public final String captchaCookie;

        public FrensChanCaptcha(@NonNull String captchaText, @NonNull String captchaCookie) {
            this.captchaCookie = captchaCookie;
            this.captchaText = captchaText;
        }
    }
}
