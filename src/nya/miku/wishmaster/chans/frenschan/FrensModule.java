package nya.miku.wishmaster.chans.frenschan;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.entity.mime.content.ByteArrayBody;
import cz.msebera.android.httpclient.message.BasicHeader;
import nya.miku.wishmaster.R;
import nya.miku.wishmaster.api.AbstractLynxChanModule;
import nya.miku.wishmaster.api.AbstractVichanModule;
import nya.miku.wishmaster.api.interfaces.CancellableTask;
import nya.miku.wishmaster.api.interfaces.ProgressListener;
import nya.miku.wishmaster.api.models.AttachmentModel;
import nya.miku.wishmaster.api.models.BoardModel;
import nya.miku.wishmaster.api.models.CaptchaModel;
import nya.miku.wishmaster.api.models.SendPostModel;
import nya.miku.wishmaster.api.models.SimpleBoardModel;
import nya.miku.wishmaster.api.models.UrlPageModel;
import nya.miku.wishmaster.api.util.ChanModels;
import nya.miku.wishmaster.common.IOUtils;
import nya.miku.wishmaster.http.ExtendedMultipartBuilder;
import nya.miku.wishmaster.http.interactive.FrensCaptchaException;
import nya.miku.wishmaster.http.streamer.HttpRequestModel;
import nya.miku.wishmaster.http.streamer.HttpResponseModel;
import nya.miku.wishmaster.http.streamer.HttpStreamer;
import nya.miku.wishmaster.lib.base64.Base64;
import nya.miku.wishmaster.lib.org_json.JSONObject;

public class FrensModule extends AbstractVichanModule {

    public static final String CHAN_NAME = "Frens Chan";
    private static final String CHAN_DOMAIN = "www.frenschan.org";
    private static final SimpleBoardModel[] BOARDS = new SimpleBoardModel[] {
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "0sint", "0sint", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "a", "Anime & Manga", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "b", "Random, Shitposts & Memes", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "biz", "Business, Finance & Crypto", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "ck", "Food & Cooking", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "coz", "Cozy Posting", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "fc", "Frens Chan Suggestions", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "fit", "Fitness", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "g", "Technology", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "h", "Homesteading, Prepping & Outdoors", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "his", "History & Humanities", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "int", "International", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "k", "Kommando", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "lit", "Literature", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "pol", "Politically Incorrect", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "r", "Religion", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "sh", "Science & Health", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "sig", "Self Improvement General", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "t", "Torrents", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "tv", "TV, Films & Music", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "v", "Vidya Games", "", false),
            ChanModels.obtainSimpleBoardModel(CHAN_NAME, "x", "Paranormal", "", false),
    };
    private static final String CAPTCHA_URL = "https://www.frenschan.org/securimage.php?mode=get&extra=abcdefghijklmnopqrstuvwxyz";

    @Nullable
    private String previousCaptchaCookie = null;

    public FrensModule(SharedPreferences preferences, Resources resources) {
        super(preferences, resources);
    }

    @Override
    public String getChanName() {
        return CHAN_NAME;
    }

    @Override
    public String getDisplayingName() {
        return "FrensChan";
    }

    @Override
    public Drawable getChanFavicon() {
        return ResourcesCompat.getDrawable(resources, R.drawable.favicon_frenschan, null);
    }

    @Override
    protected String getUsingDomain() {
        return CHAN_DOMAIN;
    }

    @Override
    protected boolean canHttps() {
        return true;
    }

    @Override
    protected SimpleBoardModel[] getBoardsList() {
        return BOARDS;
    }

    @Override
    public BoardModel getBoard(String shortName, ProgressListener listener, CancellableTask task) throws Exception {
        BoardModel model = super.getBoard(shortName, listener, task);
        model.attachmentsMaxCount = 1;
        model.allowCustomMark = true;
        model.customMarkDescription = "Spoiler";
        return model;
    }

    @Override
    protected AttachmentModel mapAttachment(JSONObject object, String boardName, boolean isSpoiler) {
        AttachmentModel attachment = super.mapAttachment(object, boardName, isSpoiler);
        if (attachment != null && attachment.type != AttachmentModel.TYPE_VIDEO) {
            attachment.thumbnail = attachment.path;
        }
        return attachment;
    }

    @Override
    public String sendPost(SendPostModel model, ProgressListener listener, CancellableTask task) throws Exception {
        FrensChanCaptchaSolved.FrensChanCaptcha captcha = FrensChanCaptchaSolved.pop();
        final String captchaText;
        final String captchaCookie;
        if (captcha != null) {
            captchaText = captcha.captchaText;
            captchaCookie = captcha.captchaCookie;
        } else {
            captchaText = model.captchaAnswer;
            captchaCookie = previousCaptchaCookie;
            previousCaptchaCookie = null;
        }
        if (captchaText == null || captchaCookie == null) {
            throw new FrensCaptchaException();
        }
        return sendPost(model, captchaText, captchaCookie, listener, task);
    }

    private String sendPost(
            SendPostModel model,
            String captchaText,
            String captchaCookie,
            ProgressListener listener,
            CancellableTask task
    ) throws Exception {
        // Copied from AbstractVichanModule with added support for the Captcha and cookie handling
        UrlPageModel urlModel = new UrlPageModel();
        urlModel.chanName = getChanName();
        urlModel.boardName = model.boardName;
        if (model.threadNumber == null) {
            urlModel.type = UrlPageModel.TYPE_BOARDPAGE;
            urlModel.boardPage = UrlPageModel.DEFAULT_FIRST_PAGE;
        } else {
            urlModel.type = UrlPageModel.TYPE_THREADPAGE;
            urlModel.threadNumber = model.threadNumber;
        }
        String referer = buildUrl(urlModel);
        List<Pair<String, String>> fields = VichanAntiBot.getFormValues(referer, task, httpClient);
        // Captcha support
        fields.add(11, new ImmutablePair<>("captcha_text", ""));
        fields.add(12, new ImmutablePair<>("captcha_cookie", ""));
        fields.add(new ImmutablePair<>("json_response", "1"));

        if    (task != null && task.isCancelled()) throw new Exception("interrupted");

        ExtendedMultipartBuilder postEntityBuilder = ExtendedMultipartBuilder.create().
                setCharset(Charset.forName("UTF-8")).setDelegates(listener, task);
        for (Pair<String, String> pair : fields) {
            if (pair.getKey().equals("spoiler") && !model.custommark) continue;
            String val;
            switch (pair.getKey()) {
                case "name": val = model.name; break;
                case "email": val = getSendPostEmail(model); break;
                case "subject": val = model.subject; break;
                case "body": val = model.comment; break;
                case "password": val = model.password; break;
                case "spoiler": val = "on"; break;
                case "captcha_text": val = captchaText; break;
                case "captcha_cookie": val = captchaCookie; break;
                default: val = pair.getValue();
            }
            if (pair.getKey().equals("file")) {
                if (model.attachments != null && model.attachments.length > 0) {
                    for (int i=0; i<model.attachments.length && i<ATTACHMENT_KEYS.length; ++i) {
                        postEntityBuilder.addFile(AbstractVichanModule.ATTACHMENT_KEYS[i], model.attachments[i], model.randomHash);
                    }
                } else {
                    postEntityBuilder.addPart(pair.getKey(), new ByteArrayBody(new byte[0], ""));
                }
            } else {
                postEntityBuilder.addString(pair.getKey(), val);
            }
        }

        String url = getUsingUrl() + "post.php";
        Header[] customHeaders = new Header[] { new BasicHeader(HttpHeaders.REFERER, referer) };
        HttpRequestModel request =
                HttpRequestModel.builder().setPOST(postEntityBuilder.build()).setCustomHeaders(customHeaders).setNoRedirect(true).build();
            HttpResponseModel response = null;
        try {
            response = HttpStreamer.getInstance().getFromUrl(url, request, httpClient, listener, task);
            if (response.statusCode == 200 || response.statusCode == 400) {
                ByteArrayOutputStream output = new ByteArrayOutputStream(1024);
                IOUtils.copyStream(response.stream, output);
                String htmlResponse = output.toString("UTF-8");
                Matcher errorMatcher = AbstractVichanModule.ERROR_PATTERN.matcher(htmlResponse);
                if (errorMatcher.find())
                    throw new Exception(errorMatcher.group(1));
            } else if (response.statusCode == 303) {
                for (Header header : response.headers) {
                    if (header != null && HttpHeaders.LOCATION.equalsIgnoreCase(header.getName())) {
                        return fixRelativeUrl(header.getValue());
                    }
                }
            }
        } finally {
            if (response != null) response.release();
        }
        return null;
    }

    @Override
    public AbstractLynxChanModule.ExtendedCaptchaModel getNewCaptcha(String boardName, String threadNumber, ProgressListener listener, CancellableTask task) throws Exception {
        Bitmap captchaBitmap;
        String captchaId;
        HttpRequestModel requestModel = HttpRequestModel.builder().setGET().setNoRedirect(true).build();
        HttpResponseModel responseModel = null;
        try {
            responseModel = performNetworkRequest(CAPTCHA_URL, requestModel, httpClient, listener, task);
            BufferedReader jsonStream = new BufferedReader(new InputStreamReader(responseModel.stream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = jsonStream.readLine()) != null) {
                stringBuilder.append(line);
            }
            JSONObject json = new JSONObject(stringBuilder.toString());

            captchaId = json.getString("cookie");

            String bitmapUrl = json.getString("captchahtml");
            int startIndex = bitmapUrl.indexOf(",") + 1;
            int endIndex = bitmapUrl.indexOf("\">");
            String parsedBitmapData = bitmapUrl.substring(startIndex, endIndex);

            byte[] image = Base64.decode(parsedBitmapData, Base64.DEFAULT);
            captchaBitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
        } finally {
            if (responseModel != null) {
                responseModel.release();
            }
        }
        AbstractLynxChanModule.ExtendedCaptchaModel captchaModel = new AbstractLynxChanModule.ExtendedCaptchaModel();
        captchaModel.type = CaptchaModel.TYPE_NORMAL;
        captchaModel.bitmap = captchaBitmap;
        captchaModel.captchaID = captchaId;
        previousCaptchaCookie = captchaId;
        return captchaModel;
    }

    private HttpResponseModel performNetworkRequest(String url, HttpRequestModel request, HttpClient client, ProgressListener listener, CancellableTask task) throws Exception {
        HttpResponseModel responseModel =  HttpStreamer.getInstance().getFromUrl(url, request, httpClient, listener, task);
        if (responseModel.statusCode == 301 || responseModel.statusCode == 302) {
            String newUrl = fixRelativeUrl(responseModel.locationHeader);
            responseModel.release();
            responseModel = HttpStreamer.getInstance().getFromUrl(newUrl, request, httpClient, listener, task);
        }
        return responseModel;
    }

    @Override
    public UrlPageModel parseUrl(String url) throws IllegalArgumentException {
        UrlPageModel model = super.parseUrl(url);
        model.boardName = Uri.decode(model.boardName);
        return model;
    }
}
