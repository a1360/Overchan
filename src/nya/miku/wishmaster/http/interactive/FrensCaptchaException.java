package nya.miku.wishmaster.http.interactive;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import nya.miku.wishmaster.api.AbstractLynxChanModule;
import nya.miku.wishmaster.api.interfaces.CancellableTask;
import nya.miku.wishmaster.chans.frenschan.FrensChanCaptchaSolved;
import nya.miku.wishmaster.chans.frenschan.FrensModule;
import nya.miku.wishmaster.common.MainApplication;

public class FrensCaptchaException extends SimpleCaptchaException {

    @NonNull
    private String previousCookie = "";

    // Example Captcha:
    @Override
    protected Bitmap getNewCaptcha() throws Exception {
        AbstractLynxChanModule.ExtendedCaptchaModel captcha =
                ((FrensModule) MainApplication.getInstance()
                        .getChanModule(FrensModule.CHAN_NAME))
                        .getNewCaptcha(null, null, null, new CancellableTask() {
                            @Override
                            public void cancel() {
                            }

                            @Override
                            public boolean isCancelled() {
                                return false;
                            }
                        });
        previousCookie = captcha.captchaID;
        return captcha.bitmap;
    }

    @Override
    protected void storeResponse(String response) {
        FrensChanCaptchaSolved.push(response, previousCookie);
    }
}
